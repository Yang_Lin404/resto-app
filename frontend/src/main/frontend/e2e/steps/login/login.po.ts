import { $, browser, ElementFinder, by } from 'protractor';

export class LoginPage {
    readonly pathUrl = '/login';
    private loginForm: ElementFinder;
    private usernameField: ElementFinder;
    private usernameErrorMsgLbl: ElementFinder;
    private passwordErrorMsgLbl: ElementFinder;
    private passwordField: ElementFinder;
    private loginBtn: ElementFinder;
    private logoutBtn: ElementFinder;
    private userDetails: ElementFinder;

    constructor() {
        this.loginForm = $('.form-login');
        this.usernameField = $('#username');
        this.passwordField = $('#password');
        this.usernameErrorMsgLbl = $('#usernameErrorMsg');
        this.passwordErrorMsgLbl = $('#passwordErrorMsg');
        this.loginBtn = $('#loginBtn');
        this.logoutBtn = $('#logoutBtn');
        this.userDetails = $('#userDetails');
    }

    navigateTo() {
        return browser.get(this.pathUrl);
    }

    public fillUsername(text: string) {
        this.usernameField.sendKeys(text);
    }

    public clickOnUsernameInput() {
        return this.usernameField.click();
    }

    public getUsernameInput() {
        return this.usernameField;
    }

    public fillPassword(text: string) {
        return this.passwordField.sendKeys(text);
    }

    public clickOnPasswordInput() {
        return this.passwordField.click();
    }

    public getPasswordInput() {
        return this.passwordField;
    }

    public isUsernameErrorMsgPresent() {
        return this.usernameErrorMsgLbl.isPresent();
    }

    public isPasswordErrorMsgPresent() {
        return this.passwordErrorMsgLbl.isPresent();
    }

    public clickOnLoginBtn() {
         return this.loginBtn.click();
    }

    public clickOnLogoutBtn() {
        return this.logoutBtn.click();
    }

    public getLoginBtn() {
        return this.loginBtn;
    }

    public getLogoutBtn() {
        return this.logoutBtn;
    }

    public isUserDetailsDisplayed() {
        return this.userDetails.isDisplayed();
    }

    public isLoginFormDisplayed() {
        return this.loginForm.isDisplayed();
    }

    public getUserDetails() {
        return this.userDetails;
    }

    public getUsername() {
        return this.userDetails.$('label').getText();
    }
}
