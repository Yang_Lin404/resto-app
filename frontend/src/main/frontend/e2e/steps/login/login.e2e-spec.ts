import {LoginPage} from './login.po';
import {browser, ExpectedConditions} from 'protractor';

const { Given, When, Then, Before } = require('cucumber');
const chai = require('chai').use(require('chai-as-promised'));
const expect = chai.expect;

const loginPage: LoginPage = new LoginPage();

Given(/^I am on login page$/, async () => {
  await loginPage.navigateTo();
});

When(/^I fill in username with "(.*?)"$/, async (text: string) => {
  await loginPage.fillUsername(text);
});

When(/^I click on username input field$/, async () => {
  await browser.actions().mouseMove(loginPage.getUsernameInput()).perform();
  await loginPage.clickOnUsernameInput();
});

When(/^I click on password input field$/, async () => {
  await browser.actions().mouseMove(loginPage.getPasswordInput()).perform();
  await loginPage.clickOnPasswordInput();
});

When(/^I fill in password with "(.*?)"$/, async (text: string) => {
  await loginPage.fillPassword(text);
});

When(/^I click on login button$/, async () => {
  await browser.actions().mouseMove(loginPage.getLoginBtn()).perform();
  await loginPage.clickOnLoginBtn();
});

Then(/^I am logged in with username "(.*?)"$/, async (expectedUsername) => {
  await browser.wait(ExpectedConditions.presenceOf(loginPage.getUserDetails()), 5000, 'login not succes');
  await expect(loginPage.isUserDetailsDisplayed()).to.eventually.be.true;
  await expect(loginPage.getUsername()).to.eventually.have.string(expectedUsername);
});

When(/^I click on logout button$/, async () => {
  await browser.actions().mouseMove(loginPage.getLogoutBtn()).perform();
  await loginPage.clickOnLogoutBtn();
});

Then(/^The login form is shown$/, async () => {
  await expect(loginPage.isLoginFormDisplayed()).to.eventually.be.true;
});

Then(/^I (can|cannot) see username error message$/, async (value: string) => {
  if (value === 'can') {
    await expect(loginPage.isUsernameErrorMsgPresent()).to.eventually.be.true;
  } else {
    await expect(loginPage.isUsernameErrorMsgPresent()).to.eventually.be.false;
  }
});

Then(/^I (can|cannot) see password error message$/, async (value: string) => {
  if (value === 'can') {
    await expect(loginPage.isPasswordErrorMsgPresent()).to.eventually.be.true;
  } else {
    await expect(loginPage.isPasswordErrorMsgPresent()).to.eventually.be.false;
  }
});
