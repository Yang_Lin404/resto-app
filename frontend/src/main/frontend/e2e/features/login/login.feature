Feature: As a user I want to be able to login and logout.

@Login
Scenario: successful login as an Admin user
  Given I am on login page
  When I fill in username with "admin"
  And I fill in password with "6211626"
  And I click on login button
  Then I am logged in with username "admin"

@Login @Logout
Scenario: successful log out after logged in
  Given I am on login page
  When I fill in username with "admin"
  And I fill in password with "6211626"
  And I click on login button
  Then I am logged in with username "admin"
  When I click on logout button
  Then The login form is shown

@Login
Scenario: I will see error messages when I enter empty username and/or password
  Given I am on login page
  When I fill in username with ""
  And I click on password input field
  Then I can see username error message
  When I fill in username with "admin"
  And I click on password input field
  Then I cannot see username error message
  When I fill in password with ""
  And I click on username input field
  Then I can see password error message
  When I fill in password with "admin"
  And I click on username input field
  Then I cannot see username error message
