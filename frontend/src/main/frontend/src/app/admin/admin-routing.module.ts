import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AdminComponent} from './admin.component';
import {UserAuthGuard} from '../core/routeGuards/user-auth-guard.service';
import {AdminRoleGuardService} from '../core/routeGuards/admin-role-guard.service';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    canActivate: [UserAuthGuard,AdminRoleGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
