export class Product {
  id: string;
  quicklink: string;
  name: string;
  ch_name: string;
  price: number;
  type: string;

  constructor(id: string, quicklink: string, name: string, ch_name: string, price: number, type: string) {
    this.id = id;
    this.quicklink = quicklink;
    this.name = name;
    this.ch_name = ch_name;
    this.price = price;
    this.type = type;
  }

  public static makeCopy(p: Product): Product {
    return new Product(p.id, p.quicklink, p.name, p.ch_name, p.price, p.type);
  }

  public static newProduct() {
    return new Product('', '', '', '', 0, 'Drink');
  }

  public static create(p: Product) {
    return new Product(p.id, p.quicklink, p.name, p.ch_name, p.price, p.type);
  }

  public equals(o2: Product): boolean {
    if (this.id === o2.id && this.quicklink === o2.quicklink && this.type === o2.type && this.name === o2.name
      && this.ch_name === o2.ch_name && this.price === o2.price) {
        return true;
      }
    return false;
  }
}
