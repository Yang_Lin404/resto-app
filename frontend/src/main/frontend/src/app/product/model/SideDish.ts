export class SideDish {
  id: string;
  name: string;
  price: number;

  constructor(id: string, name: string, price: number) {
    this.id = id;
    this.name = name;
    this.price = price;
  }

  public static create(sideDish: SideDish) {
    return new SideDish(sideDish.id, sideDish.name, sideDish.price);
  }

  public static newSideDish() {
    return new SideDish(null, '', 0);
  }
}
