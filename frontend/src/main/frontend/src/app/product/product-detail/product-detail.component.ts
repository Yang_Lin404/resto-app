import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Product} from '../model/Product';
import {ProductService} from '../../shared/services/product.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import {AlertService} from '../../core/alert/alert.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {

  @Input()
  public product: Product;
  @Input()
  public newProduct: boolean;

  productTypes: string[];
  private copyOfProduct: Product;
  form: FormGroup;
  constructor(
    public activeModal: NgbActiveModal,
    private _productService: ProductService,
    private _alertService: AlertService,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    this._productService.getAllProductTypes()
      .subscribe(types => {
        this.productTypes = types;
      });
    this.copyOfProduct = Product.makeCopy(this.product);

    this.form = this.formBuilder.group({
      quicklink:  [this.product.quicklink],
      price: [this.product.price],
      name: [this.product.name],
      ch_name: [this.product.ch_name],
      type: [this.product.type]
    });
  }

  selectType(type: string) {
    this.form.controls['type'].setValue(type);
    this.form.updateValueAndValidity();
  }

  saveProduct() {
    let productToSave = new Product(this.product.id, this.form.controls['quicklink'].value,
    this.form.controls['name'].value, this.form.controls['ch_name'].value,
    this.form.controls['price'].value, this.form.controls['type'].value);
    if (!this.copyOfProduct.equals(productToSave)) {
      this._productService.updateProduct(productToSave)
        .subscribe((resp: Product) => {
          productToSave.id = resp.id;
            this.activeModal.close(productToSave);
          }, error => {
            productToSave = this.copyOfProduct;
            this.activeModal.close(null);
            this._alertService.error(error);
          }
        );
    } else {
      this.activeModal.close(null);
    }
  }
}
