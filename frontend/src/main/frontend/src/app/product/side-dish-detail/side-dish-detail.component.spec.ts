import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideDishDetailComponent } from './side-dish-detail.component';

describe('SideDishDetailComponent', () => {
  let component: SideDishDetailComponent;
  let fixture: ComponentFixture<SideDishDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SideDishDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideDishDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
