import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UserAuthGuard} from '../core/routeGuards/user-auth-guard.service';
import {ProductComponent} from './product.component';

const routes: Routes = [
  {
    path: '',
    component: ProductComponent,
    canActivate: [UserAuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductRoutingModule { }
