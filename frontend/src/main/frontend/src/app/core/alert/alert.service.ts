import { Injectable } from '@angular/core';
import {Alert, AlertType} from './alert';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';
import {debounceTime} from 'rxjs/operator/debounceTime';

@Injectable()
export class AlertService {

  private subject = new Subject<Alert>();

  constructor() {
    debounceTime.call(this.subject, 5000).subscribe(() => this.clear());
  }

  getAlert(): Observable<Alert> {
    return this.subject.asObservable();
  }

  success(message: string) {
    this.alert(AlertType.Success, message);
  }

  info(message: string) {
    this.alert(AlertType.Info, message);
  }

  warning(message: string) {
    this.alert(AlertType.Warning, message);
  }

  error(message: string) {
    this.alert(AlertType.Error, message);
  }

  alert(type: AlertType, message: string) {
    this.subject.next(<Alert>{ type: type, message: message });
  }

  clear() {
    this.subject.next();
  }

}
