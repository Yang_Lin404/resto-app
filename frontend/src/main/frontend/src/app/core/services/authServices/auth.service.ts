import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AppConfig} from '../../../shared/configs/AppConfig';
import * as jwt_decode from 'jwt-decode';

@Injectable()
export class AuthService {

  constructor(private _httpClient: HttpClient) {
  }

  isAuthenticated(): boolean {
    return localStorage.getItem(AppConfig.ACCESS_TOKEN) !== null;
  }

  obtainAccessToken(loginData) {
    return this._httpClient.post(AppConfig.getApiEndpoint() + '/login', loginData, {
      headers: new HttpHeaders({'Content-Type':  'application/json'}),
      responseType: 'text',
      observe: 'response'
    });
  }

  getToken(): string {
    return localStorage.getItem(AppConfig.ACCESS_TOKEN);
  }

  isTokenExpired(): boolean {
    const tokenInfo = jwt_decode(this.getToken());
    return tokenInfo.exp < (Date.now() / 1000);
  }

  clearToken() {
    localStorage.removeItem(AppConfig.ACCESS_TOKEN);
  }

  saveAccessToken(header) {
    const token = header.split(' ')[1];
    localStorage.setItem(AppConfig.ACCESS_TOKEN, token);
  }
}
