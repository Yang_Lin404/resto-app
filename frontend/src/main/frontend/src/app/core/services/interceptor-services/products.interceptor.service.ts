import { Injectable } from '@angular/core';
import { Product } from '../../../product/model/Product';

@Injectable()
export class ProductInterceptorService {

    products: Product[] = [];
    productTypes = {
        Drink: 'Drink',
        MainDishe: 'MainDishe',
        Soup: 'Soup',
        Entree: 'Entree',
        Dessert: 'Dessert',
        Supplement: 'Supplement',
        Menu: 'Menu'
    };

    constructor() {
        this.createProducts();
    }

    /**
     * Create some fake products
     */
    createProducts() {

        const product0 = new Product('0', '0', 'Singapore', '零', 8.85, this.productTypes.Menu);
        const product1 = new Product('1', '1', 'Cola', '一', 1.5, this.productTypes.Soup);
        const product2 = new Product('2', '2', 'Fanta', '二', 1.5, this.productTypes.Drink);
        const product3 = new Product('3', '3', 'Rijst', '三', 1.5, this.productTypes.Menu);
        const product4 = new Product('4', '4', 'Singapore', '四', 8.85, this.productTypes.Supplement);
        const product5 = new Product('5', '5', 'Cola', '五', 1.5, this.productTypes.Dessert);
        const product6 = new Product('6', '6', 'Fanta', '六', 1.5, this.productTypes.MainDishe);
        const product7 = new Product('7', '7', 'Rijst', '七', 1.5, this.productTypes.Entree);
        const product8 = new Product('8', '8', 'Bami met look en ajuin met scampi', '八', 8.85, this.productTypes.Menu);
        const product9 = new Product('9', '9', 'Super rijst', '九', 1.5, this.productTypes.Menu);
        const product10 = new Product('10', '10', 'Fanta', '十', 1.5, this.productTypes.Drink);
        const product11 = new Product('11', '11', 'Salade', '十一', 1.5, this.productTypes.Supplement);


        this.products.push(product0, product1, product2, product3,
            product4, product5, product6, product7,
            product8, product9, product10, product11);

        return this.products;
    }

    getAllproducts() {
        return this.products;
    }

    deleteProduct(id) {
        if (id > -1) {
            return this.products.splice(id, 1);
        }
    }

    getAllProductTypes() {
        const productTypesArray: String[] = [];

        // tslint:disable-next-line:forin
        for (const product in this.productTypes) {
            productTypesArray.push(product);
        }

        return productTypesArray;
    }

    createOrUpdateProduct(product: Product) {
        const quicklink = product.quicklink;

        for (const p of this.products) {
            if (p.quicklink === quicklink) {
                this.products[this.products.indexOf(p, 0)] = product;
                return product;
            }
        }

        // add id to new product (since this is normally done by Mongodb)
        product.id = this.products.length.toString() + 1;
        this.products.push(product);
        return product;
    }
}
