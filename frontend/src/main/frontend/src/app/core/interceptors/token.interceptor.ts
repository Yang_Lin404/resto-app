import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {AuthFacadeService} from '../services/authServices/auth-facade.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(private _authFacadeService: AuthFacadeService) {}

  intercept (req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (this._authFacadeService.isLoggedIn()) {
      req = req.clone({
        headers: req.headers.set('Authorization', 'Bearer ' + this._authFacadeService.getToken())
      });
    }
    return next.handle(req);
  }
}
