import { NgModule } from '@angular/core';

import { TakeawayRoutingModule } from './takeaway-routing.module';
import { TakeawayComponent } from './takeaway.component';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
    SharedModule,
    TakeawayRoutingModule
  ],
  declarations: [TakeawayComponent]
})
export class TakeawayModule { }
