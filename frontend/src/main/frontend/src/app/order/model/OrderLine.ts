import {Product} from '../../product/model/Product';
import {Supplement} from './Supplement';
import {isNullOrUndefined} from 'util';

export class OrderLine {
  product:Product;
  private supplements:Supplement[];
  private count:number;

  constructor(product:Product, supplements: Supplement[]=null, count=1) {
    this.product = product;
    this.supplements = supplements;
    this.count = count;
  }

  public static create(orderLine:OrderLine) {
    return new OrderLine(orderLine.product, orderLine.supplements, orderLine.count);
  }

  public static createWithProduct(product: Product) {
    return new OrderLine(product,null, 1);
  }

  public static calPrice(orderLine: OrderLine): number {
    let price = orderLine.product.price * orderLine.count;

    if (!(isNullOrUndefined(orderLine.supplements)))
    orderLine.supplements.forEach(s => {
      price += s.price;
    });

    return price;
  }
}
