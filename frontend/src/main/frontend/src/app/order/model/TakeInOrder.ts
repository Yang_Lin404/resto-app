import {Order} from './Order';

export class TakeInOrder extends Order {

  constructor(order:TakeInOrder) {
    super(order.id, order.date, order.time, order.status, order.payMethod, order.orderType, order.orderLines, order.tableNr);
  }
}
