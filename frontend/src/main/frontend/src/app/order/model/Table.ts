import {TakeInOrder} from './TakeInOrder';
import {isNullOrUndefined} from 'util';

export class Table {
  tableNr: number;
  takeInOrder: TakeInOrder;

  constructor(tableNr: number, takeInOrder=null) {
    this.tableNr = tableNr;
    this.takeInOrder = takeInOrder;
  }

  public isEmpty(): boolean {
    return isNullOrUndefined(this.takeInOrder)
  }
}
