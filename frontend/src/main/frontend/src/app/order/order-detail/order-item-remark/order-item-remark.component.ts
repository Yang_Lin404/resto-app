import { Component, OnInit } from '@angular/core';
import {OrderLine} from '../../model/OrderLine';

@Component({
  selector: 'app-order-item-remark',
  templateUrl: './order-item-remark.component.html',
  styleUrls: ['./order-item-remark.component.css']
})
export class OrderItemRemarkComponent implements OnInit {

  orderLine: OrderLine;

  constructor() { }

  ngOnInit() {
  }

}
