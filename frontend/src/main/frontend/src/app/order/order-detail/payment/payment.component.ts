import {Component, Input, OnInit} from '@angular/core';

import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {

  price: number;
  change = 0;
  amount: number;

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

  pay(payMethod: string) {
    this.activeModal.close(payMethod);
  }

  calChange() {
    if (!isNaN(this.amount)) {
      this.change = this.amount - this.price;
    }
  }
}
