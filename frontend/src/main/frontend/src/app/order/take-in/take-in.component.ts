import { Component, OnInit } from '@angular/core';
import {Table} from '../model/Table';
import {TakeInService} from '../services/take-in.service';
import {TakeInOrder} from '../model/TakeInOrder';
import {Router} from '@angular/router';
import {ConfirmWindowComponent} from '../../shared/confirm-window/confirm-window.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {AlertService} from '../../core/alert/alert.service';

@Component({
  selector: 'app-order-take-in',
  templateUrl: './take-in.component.html',
  styleUrls: ['./take-in.component.css']
})
export class TakeInComponent implements OnInit {

  private TOTAL_TABLE = 15;
  tables: Table[];
  private orders: TakeInOrder[];

  public loading = false;

  constructor(private _router: Router,
              private _takeInService: TakeInService,
              private _modalService: NgbModal,
              private _alertService: AlertService) { }

  ngOnInit() {
    this.fetchTodayOrders();
  }

  private fetchTodayOrders() {
    this.loading = true;
    this._takeInService.getTodayOrders().subscribe(
        data => {
          this.initOrders(data);
          this.initTables();
          this.loading = false;
        },
        error => console.log(error))
  }

  private initOrders(orders: TakeInOrder[]) {
    this.orders = [];
    orders.forEach(o => {
      this.orders.push(new TakeInOrder(o));
    })
  }

  private initTables() {
    this.tables = [];
    for (let i = 1; i <= this.TOTAL_TABLE; i++) {
      this.tables.push(new Table(i));
    }
    this.orders.forEach(order => {
      if (!order.isPaid()) {
        this.tables.find(t => t.tableNr === order.tableNr)
          .takeInOrder = order;
      }
    })
  }

  openOrderDetail(table: Table) {
    if (table.isEmpty()) {
      this.createOrder(table);
    } else {
      this.redirectToOrderDetail(table);
    }
  }

  private redirectToOrderDetail(table: Table) {
    this._router.navigate(['/order/orderDetail',
      {id: table.takeInOrder.id, orderType: table.takeInOrder.orderType}])
  }

  private createOrder(table: Table) {
    this._takeInService.createOrder(table.tableNr)
      .subscribe(o => {
        table.takeInOrder = o;
        this.redirectToOrderDetail(table);
      })
  }

  openRemoveOrderConfirmWindow(table: Table) {
    const modalRef = this._modalService.open(ConfirmWindowComponent);
    modalRef.componentInstance.message = 'are you sure want to delete order of table ' + table.tableNr;
    modalRef.result.then(result => {
      if (result) {
        this.removeOrder(table);
      }
    }, error => {} );
  }

  removeOrder(table: Table) {
    this._takeInService.removeOrder(table.takeInOrder.id)
      .subscribe(o => {
        table.takeInOrder = null;
        this._alertService.success('Remove order of table ' + table.tableNr + ' is succeeded.');
      })
  }
}
