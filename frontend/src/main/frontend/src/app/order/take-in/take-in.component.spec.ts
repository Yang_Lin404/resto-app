import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TakeInComponent } from './take-in.component';

describe('TakeInComponent', () => {
  let component: TakeInComponent;
  let fixture: ComponentFixture<TakeInComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TakeInComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TakeInComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
