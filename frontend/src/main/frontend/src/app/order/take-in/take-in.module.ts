import { NgModule } from '@angular/core';

import { TakeInComponent } from './take-in.component';
import {SharedModule} from '../../shared/shared.module';
import {TakeInService} from '../services/take-in.service';
import {LoadingModule} from 'ngx-loading';

@NgModule({
  imports: [
    SharedModule,
    LoadingModule
  ],

  declarations: [TakeInComponent],
  providers: [TakeInService]
})
export class TakeInModule { }
