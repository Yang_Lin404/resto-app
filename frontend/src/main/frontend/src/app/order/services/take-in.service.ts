import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AppConfig} from '../../shared/configs/AppConfig';
import {TakeInOrder} from '../model/TakeInOrder';
import {OrderLine} from '../model/OrderLine';
import {OrderType, PayMethod} from '../model/Order';
import {OrderService} from './order.service';

@Injectable()
export class TakeInService {

  private TAKE_IN_ORDER_ENDPOINT = AppConfig.getApiEndpoint() + '/api/take-in/order';

  constructor(private _httpClient: HttpClient, private _orderService: OrderService) { }

  public getTodayOrders() {
    const url = this.TAKE_IN_ORDER_ENDPOINT + '/today';
    return this._httpClient.get<TakeInOrder[]>(url);
  }

  public getOrderById(orderId:string) {
    const url = this.TAKE_IN_ORDER_ENDPOINT + '/' + orderId;
    return this._httpClient.get<TakeInOrder>(url);
  }

  public createOrder(tableNr: number) {
    return this._orderService.createOrder(tableNr);
  }

  public updatePayMethod(id: string, payMethod: PayMethod) {
    return this._orderService.updatePayMethod(id, payMethod, OrderType.TakeIn);
  }

  public addOrderLineToOrder(id: string, newOrderLine: OrderLine) {
    return this._orderService.addOrderLineToOrder(id, newOrderLine, OrderType.TakeIn);
  }

  public removeOrderLineFromOrder(id: string, orderLineId: string) {
    return this._orderService.removeOrderLineFromOrder(id, orderLineId, OrderType.TakeIn);
  }

  public removeOrder(id: string) {
    return this._orderService.removeOrder(id);
  }

  private buildDateUrlParameter(date: Date):string {
    return 'year=' + date.getFullYear() + '&month=' + date.getMonth() + '&day=' + date.getDay();
  }
}
