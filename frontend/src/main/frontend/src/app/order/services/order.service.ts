import { Injectable } from '@angular/core';
import {AppConfig} from '../../shared/configs/AppConfig';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Order, OrderType, PayMethod} from '../model/Order';
import {isNullOrUndefined} from 'util';
import {OrderLine} from '../model/OrderLine';

@Injectable()
export class OrderService {

  private ORDER_ENDPOINT = AppConfig.getApiEndpoint() + '/api/order';
  private HEADER_ORDER_TYPE = 'orderType';

  constructor(private _httpClient: HttpClient) {}

  public createOrder(tableNr?: number) {
    const url = isNullOrUndefined(tableNr) ? this.ORDER_ENDPOINT : this.ORDER_ENDPOINT + '?table=' + tableNr;
    return this._httpClient.put<Order>(url,null);
  }

  public updatePayMethod(id: string, payMethod: PayMethod, type: OrderType) {
    const url = this.constructOrderUrl(id) + '/pay/' + payMethod;
    const headers = new HttpHeaders().append(this.HEADER_ORDER_TYPE, type);
    return this._httpClient.post(url, null, {headers});
  }

  public addOrderLineToOrder(id: string, newOrderLine: OrderLine, type: OrderType) {
    const url = this.constructOrderLineUrl(id);
    const headers = new HttpHeaders().append(this.HEADER_ORDER_TYPE, type);
    return this._httpClient.put<Order>(url, newOrderLine, {headers});
  }

  public removeOrderLineFromOrder(orderId: string, orderLineId: string, type: OrderType) {
    const url = this.constructOrderLineUrl(orderId) + '/' + orderLineId;
    const headers = new HttpHeaders().append(this.HEADER_ORDER_TYPE, type);
    return this._httpClient.delete<Order>(url, {headers});
  }

  public removeOrder(id: string) {
    const url = this.constructOrderUrl(id)
    return this._httpClient.delete(url);
  }

  private constructOrderUrl(id: string) {
    return this.ORDER_ENDPOINT + '/' + id;
  }

  private constructOrderLineUrl(id: string) {
    return this.constructOrderUrl(id) + '/' + 'orderLine';
  }

}
