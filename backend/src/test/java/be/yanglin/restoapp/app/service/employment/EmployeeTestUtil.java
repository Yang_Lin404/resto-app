package be.yanglin.restoapp.app.service.employment;

import be.yanglin.restoapp.app.model.employment.Employee;
import be.yanglin.restoapp.app.model.employment.Work;
import be.yanglin.restoapp.app.restAPI.model.WorkQueryDTO;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Collections;
import java.util.List;

class EmployeeTestUtil {

    static final String WORK_ID = "1";
    static final String EMPLOYEE_ID = "1B";
    static final String EMPTY_ID = "";
    static final String START_DATE_STRING = "2018-04-24";
    static final String END_DATE_STRING = "2018-04-27";

    static Employee aEmployee() {
        Employee e = new Employee();
        e.setId("test");
        e.setFirstName("testFirstName");
        e.setLastName("testLastName");
        return e;
    }

    static Employee aNewEmployee() {
        Employee e = aEmployee();
        e.setId(EMPTY_ID);
        return e;
    }

    static List<Employee> aListOfEmployee() {
        return Collections.singletonList(aEmployee());
    }

    static Work aNewWork() {
        return aWork(EMPTY_ID);
    }

    static Work aExistWork() {
        return aWork(WORK_ID);
    }

    static Work aConfirmWork() {
        Work work = aExistWork();
        work.setConfirmed(true);
        return work;
    }

    static List<Work> aListOfExistWorks() {
        return Collections.singletonList(aWork(WORK_ID));
    }

    static Work aWork(String id) {
        LocalTime startTime = LocalTime.now();
        LocalTime endTime = startTime.plusHours(1);
        LocalDate day = LocalDate.now();
        Employee employee = aEmployee();
        return new Work(id, startTime, endTime, day, employee);
    }

    static WorkQueryDTO aWorkQueryDTO() {
        return new WorkQueryDTO(EMPLOYEE_ID, START_DATE_STRING, END_DATE_STRING);
    }
}
