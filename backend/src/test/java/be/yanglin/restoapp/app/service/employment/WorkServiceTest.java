package be.yanglin.restoapp.app.service.employment;

import be.yanglin.restoapp.app.model.employment.Employee;
import be.yanglin.restoapp.app.model.employment.Work;
import be.yanglin.restoapp.app.repo.WorkRepository;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Collections;
import java.util.List;

import static be.yanglin.restoapp.app.service.employment.EmployeeTestUtil.EMPLOYEE_ID;
import static be.yanglin.restoapp.app.service.employment.EmployeeTestUtil.aEmployee;
import static be.yanglin.restoapp.app.service.employment.EmployeeTestUtil.aExistWork;
import static be.yanglin.restoapp.app.service.employment.EmployeeTestUtil.aListOfExistWorks;
import static be.yanglin.restoapp.app.service.employment.EmployeeTestUtil.aNewWork;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class WorkServiceTest {

    @InjectMocks
    private WorkService service;

    @Mock
    private WorkRepository repository;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void getAllWorkDayOfEmployeeAndDay() {
        final LocalDate day = LocalDate.now();
        final List<Work> works = Collections.singletonList(new Work());
        when(repository.findAllByEmployeeIdAndDay(EMPLOYEE_ID, day))
                .thenReturn(works);

        final List<Work> worksActual = service.getAllWorks(EMPLOYEE_ID, day);

        assertNotNull(worksActual);
        assertEquals(works, worksActual);

        verify(repository, times(1)).findAllByEmployeeIdAndDay(EMPLOYEE_ID, day);
    }

    @Test
    public void getAllWorkOfEmployee() {
        final List<Work> weeklyWorkHours = aListOfExistWorks();

        when(repository.findAllByEmployeeId(EMPLOYEE_ID))
                .thenReturn(weeklyWorkHours);

        final List<Work> worksActual = service.getAllWorks(EMPLOYEE_ID);

        assertNotNull(worksActual);
        assertEquals(weeklyWorkHours, worksActual);

        verify(repository, times(1)).findAllByEmployeeId(EMPLOYEE_ID);
    }

    @Test
    public void getWorksOfEmployeeBetweenDays() {
        final List<Work> works = aListOfExistWorks();
        final LocalDate startDate = LocalDate.now();
        final LocalDate endDate = startDate.plusDays(2);

        when(repository.findByEmployeeIdAndDayBetween(EMPLOYEE_ID, startDate, endDate)).thenReturn(works);

        final List<Work> worksActual = service.getAllWorks(EMPLOYEE_ID, startDate, endDate);

        assertNotNull(worksActual);
        verify(repository, times(1))
                .findByEmployeeIdAndDayBetween(EMPLOYEE_ID, startDate, endDate);
    }

    @Test
    public void getWorksOfEmployeeBetweenDaysWhenStartDateAndEndDateAreSome() {
        Employee employee = aEmployee();
        final List<Work> works = aListOfExistWorks();
        final LocalDate startDate = LocalDate.now();
        final LocalDate endDate = LocalDate.now();

        when(repository.findAllByEmployeeIdAndDay(EMPLOYEE_ID, startDate)).thenReturn(works);

        final List<Work> worksActual = service.getAllWorks(EMPLOYEE_ID, startDate, endDate);

        assertNotNull(worksActual);
        assertEquals(works, worksActual);
        verify(repository, times(1)).findAllByEmployeeIdAndDay(EMPLOYEE_ID, startDate);
    }

    @Test
    public void getWorksOfEmployeeBetweenDaysWhenStartDateAfterEndDate() {
        final LocalDate startDate = LocalDate.now();
        final LocalDate endDate = startDate.minusDays(2);

        expectedException.expect(DateTimeException.class);
        service.getAllWorks(EMPLOYEE_ID, startDate, endDate);

        verify(repository, never()).findByEmployeeIdAndDayBetween(any(), any(), any());
    }

    @Test
    public void addWorkToEmployee() {
        Work work = aNewWork();

        when(repository.insert(work)).thenReturn(work);

        Work workActual = service.updateOrAddWork(work);

        assertNotNull(workActual);
        verify(repository, times(1)).insert(work);
    }

    @Test
    public void updateWorkToEmployee() {
        Work work = aExistWork();

        when(repository.save(work)).thenReturn(work);

        Work workActual = service.updateOrAddWork(work);

        assertNotNull(workActual);
        verify(repository, never()).insert(work);
        verify(repository, times(1)).save(work);
    }

    @Test
    public void updateWorks() {
        List<Work> works = aListOfExistWorks();

        when(repository.saveAll(works)).thenReturn(works);

        List<Work> worksActual = service.updateWorks(works);

        assertNotNull(worksActual);
        verify(repository, times(1)).saveAll(works);
    }

    @Test
    public void deleteWork() {
        Work work = aExistWork();

        doNothing().when(repository).delete(work);

        service.deleteWork(work);

        verify(repository, times(1)).delete(work);
    }


}