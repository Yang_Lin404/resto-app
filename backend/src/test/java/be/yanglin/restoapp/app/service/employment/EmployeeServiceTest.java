package be.yanglin.restoapp.app.service.employment;

import be.yanglin.restoapp.app.exception.EmployeeNotFoundException;
import be.yanglin.restoapp.app.model.employment.Employee;
import be.yanglin.restoapp.app.repo.EmployeeRepository;
import be.yanglin.restoapp.app.service.employment.EmployeeService;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static be.yanglin.restoapp.app.service.employment.EmployeeTestUtil.aEmployee;
import static be.yanglin.restoapp.app.service.employment.EmployeeTestUtil.aNewEmployee;
import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class EmployeeServiceTest {

    @InjectMocks
    private EmployeeService service;

    @Mock
    private EmployeeRepository repository;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void getAllEmployees() {
        when(repository.findAll()).thenReturn(Collections.singletonList(new Employee()));

        List<Employee> employeesActual = service.getAllEmployees();

        assertNotNull(employeesActual);
    }

    @Test
    public void getEmployeeById() throws Exception {
        final Employee employeeExpected = aEmployee();
        when(repository.findById("test")).thenReturn(Optional.of(employeeExpected));

        Employee employee = service.getEmployeeById("test");

        assertNotNull(employee);
        assertEquals(employeeExpected, employee);
    }

    @Test
    public void getEmployeeByIdWhenNotFound() throws Exception {
        expectedException.expect(EmployeeNotFoundException.class);
        service.getEmployeeById("notFound");
    }

    @Test
    public void getEmployeeByName() throws Exception {
        final Employee employee = aEmployee();
        when(repository.findByFirstNameAndLastName(employee.getFirstName(), employee.getLastName()))
                .thenReturn(Optional.of(employee));

        final Employee employeeActual = service.getEmployeeByName(employee.getFirstName(), employee.getLastName());

        assertNotNull(employeeActual);
        assertEquals(employee, employeeActual);
        verify(repository, times(1))
                .findByFirstNameAndLastName(employee.getFirstName(), employee.getLastName());
    }

    @Test
    public void getEmployeeByNameWhenNotFound() throws Exception {
        expectedException.expect(EmployeeNotFoundException.class);
        service.getEmployeeByName("notFound", "notFound");
    }

    @Test
    public void addNewEmployee() {
        final Employee newEmployee = aNewEmployee();

        when(repository.insert(newEmployee)).thenReturn(newEmployee);

        final Employee employeeActual = service.addNewEmployee(newEmployee);

        assertNotNull(employeeActual);
        assertEquals(newEmployee, employeeActual);
        verify(repository, times(1)).insert(newEmployee);
    }

    @Test
    public void updateEmployee() {
        final Employee employee = aEmployee();

        when(repository.save(employee)).thenReturn(employee);

        final Employee employeeActual = service.updateEmployee(employee);

        assertNotNull(employeeActual);
        assertEquals(employee, employeeActual);
        verify(repository, times(1)).save(employee);
    }

}