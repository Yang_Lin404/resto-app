package be.yanglin.restoapp;

import be.yanglin.restoapp.app.exception.ProductNotUniqueException;
import be.yanglin.restoapp.app.exception.SideDishNotNewException;
import be.yanglin.restoapp.app.exception.SideDishNotUniqueException;
import be.yanglin.restoapp.app.exception.order.OrderNotFoundException;
import be.yanglin.restoapp.app.exception.order.TableAlreadyActiveException;
import be.yanglin.restoapp.app.model.order.Order;
import be.yanglin.restoapp.app.model.order.OrderLine;
import be.yanglin.restoapp.app.model.order.OrderType;
import be.yanglin.restoapp.app.model.product.Product;
import be.yanglin.restoapp.app.model.product.ProductType;
import be.yanglin.restoapp.app.model.product.SideDish;
import be.yanglin.restoapp.app.service.product.ProductService;
import be.yanglin.restoapp.app.service.order.OrderFacade;
import be.yanglin.restoapp.app.service.product.SideDishService;
import be.yanglin.restoapp.auth.model.ApplicationUser;
import be.yanglin.restoapp.auth.model.Role;
import be.yanglin.restoapp.auth.service.ApplicationUserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.Collections;

@Component
public class StartupApplicationListener implements ApplicationListener<ContextRefreshedEvent> {

    private final static Logger LOGGER = LogManager.getLogger(StartupApplicationListener.class);

    @Autowired
    private ProductService productService;

    @Autowired
    private SideDishService sideDishService;

    @Autowired
    private ApplicationUserService applicationUserService;

    @Autowired
    private OrderFacade orderFacade;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextStartedEvent) {
        LOGGER.info("spring context started. init application");

        try {
            initStubForProducts();
            initStubForSideDishes();
            initStubForUserAcc();
            initStubForTodayTakeInOrders();
        }catch (OrderNotFoundException |
                TableAlreadyActiveException |
                SideDishNotNewException |
                SideDishNotUniqueException o) {
            LOGGER.error("error during initialisation of stub data's");
        }
    }

    private void initStubForSideDishes() throws SideDishNotNewException, SideDishNotUniqueException {
        if (sideDishService.isSidedishListEmpty()) {
            SideDish sideDish = SideDish.builder().name("friet").price(2.0d).build();
            sideDishService.addNewSideDish(sideDish);
        }
    }

    private void initStubForProducts() {
        if (productService.isProductListEmpty()) {
            Product product = new Product("test", "test", "ook test", 22, ProductType.MainDishe);
            try {
                productService.updateOrAddNewProduct(product);
            } catch (ProductNotUniqueException ex) {
                LOGGER.error("not possible");
            }
        }
    }

    private void initStubForUserAcc() {
        if (!applicationUserService.doesUserExist("admin"))
            applicationUserService.addUser(ApplicationUser.builder()
                    .username("admin")
                    .password("6211626")
                    .roles(Collections.singletonList(Role.ROLE_ADMIN))
                    .build());


        if (!applicationUserService.doesUserExist("test"))
            applicationUserService.addUser(ApplicationUser.builder()
                    .username("test")
                    .password("test")
                    .roles(Collections.singletonList(Role.ROLE_USER))
                    .build());
    }

    private void initStubForTodayTakeInOrders() throws OrderNotFoundException, TableAlreadyActiveException {
        if (orderFacade.getTakeInOrdersOfDate(LocalDate.now()).isEmpty()) {
            for (int i=1; i<=5; i++) {
                Order takeInOrder = orderFacade.createOrder(i);
                orderFacade.addOrderLine(aOrderLine("test"), takeInOrder.id, OrderType.TakeIn);
            }
        }
    }

    private OrderLine aOrderLine(String productQL) {
        return OrderLine.builder()
                .count(1)
                .product(productService.getProductByQuicklink(productQL))
                .supplements(Collections.emptyList())
                .build();
    }
}
