package be.yanglin.restoapp.app.service.product;

import be.yanglin.restoapp.app.exception.SideDishNotNewException;
import be.yanglin.restoapp.app.exception.SideDishNotUniqueException;
import be.yanglin.restoapp.app.model.product.SideDish;
import be.yanglin.restoapp.app.repo.product.SideDishRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class SideDishService {

    @Autowired
    private SideDishRepository repo;

    public List<SideDish> getAll() {
        return repo.findAll();
    }

    public SideDish addNewSideDish(SideDish sideDish) throws SideDishNotNewException, SideDishNotUniqueException {
        if (!StringUtils.isEmpty(sideDish.getId()))
            throw new SideDishNotNewException(sideDish);

        else if (isSideDishUnique(sideDish)){
            throw new SideDishNotUniqueException(sideDish);
        }

        return repo.insert(sideDish);
    }

    public void deleteSideDish(String id) {
        repo.deleteById(id);
    }

    public SideDish updateSideDish(SideDish sideDish) {
        return repo.save(sideDish);
    }

    public boolean isSidedishListEmpty() {
        return repo.count() == 0;
    }

    private boolean isSideDishUnique(SideDish sideDish) {
        return repo.existsByName(sideDish.getName());
    }
}
