package be.yanglin.restoapp.app.exception;

public class EmployeeNotFoundException extends Exception {

    public EmployeeNotFoundException(String id) {
        super("worker with id " + id + " cannot be found.");
    }

    public EmployeeNotFoundException(String firstName, String lastName) {
        super("worker with name " + firstName + lastName + " cannot be found.");
    }
}
