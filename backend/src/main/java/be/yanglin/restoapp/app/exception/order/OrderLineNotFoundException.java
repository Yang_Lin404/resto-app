package be.yanglin.restoapp.app.exception.order;

public class OrderLineNotFoundException extends Exception {

    public OrderLineNotFoundException(String orderId, String productId) {
        super("OrderLine with productId: " + productId + " not found in order " + orderId);
    }
}
