package be.yanglin.restoapp.app.restAPI.controller.order;

import be.yanglin.restoapp.app.exception.order.OrderNotFoundException;
import be.yanglin.restoapp.app.model.order.TakeInOrder;
import be.yanglin.restoapp.app.service.order.OrderFacade;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/api/take-in/order")
public class TakeInOrderController {

    private final static Logger LOGGER = LogManager.getLogger(TakeInOrderController.class);

    @Autowired
    private OrderFacade orderFacade;

    @GetMapping
    public List<TakeInOrder> getTakeInOrders() {
        return orderFacade.getTakeInOrders();
    }

    @GetMapping("/date")
    public List<TakeInOrder> getTakeInOrdersByDate(@RequestParam("year") int year,
                                                   @RequestParam("month") int month,
                                                   @RequestParam("day") int day) {
        LocalDate date = LocalDate.of(year, month, day);
        return orderFacade.getTakeInOrdersOfDate(date);
    }

    @GetMapping("/today")
    public List<TakeInOrder> getTodayTakeInOrders() {
        return orderFacade.getTakeInOrdersOfDate(LocalDate.now());
    }

    @GetMapping("/{id}")
    public TakeInOrder getTakeInOrderById(@PathVariable("id") String id) throws OrderNotFoundException {
        LOGGER.info("get take in order by id: " + id);
        return orderFacade.findTakeInOrder(id);
    }
}
