package be.yanglin.restoapp.app.repo.order;

import be.yanglin.restoapp.app.model.order.Order;
import be.yanglin.restoapp.app.model.order.TakeInOrder;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface TakeInOrderRepository extends MongoRepository<TakeInOrder, String>{
    List<TakeInOrder> findAllByDate(LocalDate date);
    Optional<TakeInOrder> findByDateAndTableNrAndStatus(LocalDate date, int TableNr, Order.Status status);
}
