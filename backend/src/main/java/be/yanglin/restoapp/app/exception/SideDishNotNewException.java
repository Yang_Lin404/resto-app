package be.yanglin.restoapp.app.exception;

import be.yanglin.restoapp.app.model.product.SideDish;

public class SideDishNotNewException extends Exception {

    public SideDishNotNewException(SideDish sideDish) {
        super("side dish: " + sideDish + " is not new.");
    }
}
