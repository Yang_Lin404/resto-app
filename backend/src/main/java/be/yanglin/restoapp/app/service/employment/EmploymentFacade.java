package be.yanglin.restoapp.app.service.employment;

import be.yanglin.restoapp.app.exception.EmployeeNotFoundException;
import be.yanglin.restoapp.app.model.employment.Employee;
import be.yanglin.restoapp.app.model.employment.Work;
import be.yanglin.restoapp.app.restAPI.model.WorkQueryDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.List;

@Service
public class EmploymentFacade {

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private WorkService workService;

    public List<Work> getAllWorksOfEmployee(String employeeId) {
        return workService.getAllWorks(employeeId);
    }

    public List<Work> getAllWorksOfEmployeeByDay(String employeeId, String day) {
        LocalDate date = LocalDate.parse(day);
        return workService.getAllWorks(employeeId, date);
    }

    public List<Employee> getAllEmployee() {
        return employeeService.getAllEmployees();
    }

    public Employee getEmployeeById(String id) throws EmployeeNotFoundException {
        return employeeService.getEmployeeById(id);
    }

    public Employee getEmployeeByName(String firstName, String lastName) throws EmployeeNotFoundException {
        return employeeService.getEmployeeByName(firstName, lastName);
    }

    public List<Work> getWorksOfEmployeeOfWeek(WorkQueryDTO workQueryDTO)
            throws DateTimeException {
        LocalDate startDate = LocalDate.parse(workQueryDTO.getStartDate());
        LocalDate endDate = LocalDate.parse(workQueryDTO.getEndDate());
        return workService.getAllWorks(workQueryDTO.getEmployeeId(), startDate, endDate);
    }

    public Employee addNewEmployee(Employee employee) {
        return employeeService.addNewEmployee(employee);
    }

    public Employee updateEmployee(Employee employee) {
        return employeeService.updateEmployee(employee);
    }

    public Work addNewWork(Work work) {
        return workService.updateOrAddWork(work);
    }

    public void confirmWork(String workId) {

    }
}
