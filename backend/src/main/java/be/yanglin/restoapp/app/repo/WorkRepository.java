package be.yanglin.restoapp.app.repo;

import be.yanglin.restoapp.app.model.employment.Work;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.time.LocalDate;
import java.util.List;

public interface WorkRepository extends MongoRepository<Work, String> {

    List<Work> findAllByEmployeeIdAndDay(String employeeId, LocalDate work);

    List<Work> findAllByEmployeeId(String employee);

    List<Work> findByEmployeeIdAndDayBetween(String employeeId, LocalDate start, LocalDate end);
}
