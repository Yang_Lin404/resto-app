package be.yanglin.restoapp.app.exception;

import be.yanglin.restoapp.app.model.product.SideDish;

public class SideDishNotUniqueException extends Exception {

    public SideDishNotUniqueException(SideDish sideDish) {
        super("Side Dish: " + sideDish + " is not unique.");
    }
}
