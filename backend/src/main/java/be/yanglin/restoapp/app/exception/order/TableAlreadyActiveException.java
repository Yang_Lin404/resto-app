package be.yanglin.restoapp.app.exception.order;

public class TableAlreadyActiveException extends Exception {
    public TableAlreadyActiveException(int table) {
        super("Table " + table + " is active");
    }
}
