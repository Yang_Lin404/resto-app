package be.yanglin.restoapp.app.service.product;

import be.yanglin.restoapp.app.exception.ProductNotUniqueException;
import be.yanglin.restoapp.app.model.product.Product;
import be.yanglin.restoapp.app.model.product.ProductType;
import be.yanglin.restoapp.app.repo.product.ProductRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class ProductService {

    private final static Logger LOGGER = LogManager.getLogger(ProductService.class);

    @Autowired
    private ProductRepository productRepository;

    public List<Product> getAllProduct() {
        return productRepository.findAll();
    }

    public List<ProductType> getAllProductTypes() { return Arrays.asList(ProductType.values());}

    public Product getProductById(String id) {
        return productRepository.findById(id).orElse(null);
    }

    public Product getProductByQuicklink(String ql) {
        return productRepository.findByQuicklink(ql).orElse(null);
    }

    public Product updateOrAddNewProduct(Product product) throws ProductNotUniqueException {
        if (this.isANewProduct(product) && (!isProductUnique(product))) {
            LOGGER.error("try to add a new product with not unique quicklink: " + product);
            throw new ProductNotUniqueException();
        } else if (this.isANewProduct(product)) {
            LOGGER.debug("product with quicklink: " + product.getQuicklink() + " is a new product, set id to null");
            product.setId(null);
        }
        return productRepository.save(product);
    }

    public void deleteProduct(String id) {
        this.productRepository.deleteById(id);
    }

    public boolean isProductListEmpty() {
        return productRepository.count() == 0;
    }

    boolean isANewProduct(Product product) {
        return product.getId() != null && product.getId().isEmpty();
    }

    boolean isProductUnique(Product p) {
        return !this.productRepository.existsByQuicklink(p.getQuicklink());
    }
}
