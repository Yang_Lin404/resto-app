package be.yanglin.restoapp.app.model.employment;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class Work {

    @Id
    private String id;

    private LocalTime startTime;
    private LocalTime endTime;
    private LocalDate day;
    private boolean confirmed;
    private boolean paid;

    @DBRef
    private Employee employee;

    public Work(String id, LocalTime startTime, LocalTime endTime, LocalDate day, Employee employee) {
        this.id = id;
        this.startTime = startTime;
        this.endTime = endTime;
        this.day = day;
        this.employee = employee;
        this.confirmed = false;
        this.paid = false;
    }
}
