package be.yanglin.restoapp.auth.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import static be.yanglin.restoapp.auth.security.SecurityConstants.HEADER_STRING;
import static be.yanglin.restoapp.auth.security.SecurityConstants.SECRET;
import static be.yanglin.restoapp.auth.security.SecurityConstants.TOKEN_PREFIX;


public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

    public JWTAuthorizationFilter(AuthenticationManager authManager) {
        super(authManager);
    }

    @Override
    protected void doFilterInternal(HttpServletRequest req,
                                    HttpServletResponse res,
                                    FilterChain chain) throws IOException, ServletException {
        String header = req.getHeader(HEADER_STRING);

        if (header == null || !header.startsWith(TOKEN_PREFIX)) {
            chain.doFilter(req, res);
            return;
        }

        UsernamePasswordAuthenticationToken authentication = getAuthentication(req);

        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(req, res);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
        String token = request.getHeader(HEADER_STRING);
        if (token != null) {
            // parse the token.
            Claims claims = Jwts.parser()
                    .setSigningKey(SECRET.getBytes())
                    .parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
                    .getBody();
            if (claims.getSubject() != null) {
                List<GrantedAuthority> authorities =
                        getGrantedAuthorities((ArrayList<LinkedHashMap<String,String>>) claims.get("authorities"));
                return new UsernamePasswordAuthenticationToken(claims.getSubject(), null, authorities);
            }
            return null;
        }
        return null;
    }

    private List<GrantedAuthority> getGrantedAuthorities(ArrayList<LinkedHashMap<String,String>> authorityMaps) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (LinkedHashMap<String,String> authority : authorityMaps) {
            authorities.add(new SimpleGrantedAuthority(authority.get("authority")));
        }
        return authorities;
    }
}
