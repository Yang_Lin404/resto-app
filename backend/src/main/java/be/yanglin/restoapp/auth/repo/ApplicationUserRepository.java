package be.yanglin.restoapp.auth.repo;

import be.yanglin.restoapp.auth.model.ApplicationUser;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ApplicationUserRepository extends CrudRepository<ApplicationUser, String> {

    ApplicationUser findByUsername(String username);
    boolean existsByUsername(String username);
    @Override
    List<ApplicationUser> findAll();
}
